package PracticeDay1;

import java.util.HashSet;
import java.util.Set;

public class HashSetProgram {

	public static void main(String[] args) {
		Set<String> obj = new HashSet<>();

		// add()
		obj.add("pradeep");
		obj.add("yeswanth");
		obj.add("ram");
		System.out.println(obj);
		boolean contains = obj.contains("pradeep");
		System.out.println(contains);

		int size = obj.size();
		System.out.println(size);

		obj.remove(0);
		System.out.println(obj);

	}
}
