package PracticeDay1;

public class SentenceCheck {

	public static void main(String[] args) {
		String sentence = "Welcome to Globallogic";
		String  word = "Globallogic";
		if (sentence.contains(word)) {
			System.out.println("Globallogic word present in this sentence");
		} else {
			System.out.println("Globallogic word is not present in this sentence");
		}
	}

}
