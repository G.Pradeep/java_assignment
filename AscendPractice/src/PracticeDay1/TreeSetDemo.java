package PracticeDay1;

import java.util.TreeSet;

public class TreeSetDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TreeSet<Integer> tree = new TreeSet<Integer>();
		tree.add(10);
		tree.add(30);
		tree.add(50);
		tree.add(60);
		tree.add(70);
		System.out.println(tree);
		boolean b = tree.contains(20);
		System.out.println(b);
		tree.remove(30);
		System.out.println(tree);
	}

}
