package PracticeDay1;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class HashMapProgram {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<Integer, String> nam = new HashMap<Integer, String>();
		nam.put(0, "java");
		nam.put(1, "selenium");
		nam.put(2, "Project");
		nam.put(3, "Testing");
		nam.put(4, "java");
		nam.put(5, "jaww");
		nam.put(6, "ja");
		nam.put(7, "java");
		int size = nam.size();
		System.out.println(size);
		String string = nam.get(5);
		System.out.println(string);
		Set<Integer> keySet = nam.keySet();
		System.out.println(keySet);
		String remove = nam.remove(4);
		System.out.println(remove);
		nam.replace(5, "java");
		System.out.println(nam);

	}

}
