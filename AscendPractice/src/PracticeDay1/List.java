package PracticeDay1;

import java.util.ArrayList;

public class List {
	
public static void main(String[] args) {
	ArrayList<String> list = new ArrayList<>();
	
	list.add("a");
	list.add("b");
	list.add("c");
	list.add("d");
	list.add("d");
	System.out.println(list.size());//element stored in consecutive memory location
	list.set(2, "java");
	 System.out.println(list);
	 list.remove(0);
	 System.out.println(list);
}
}
