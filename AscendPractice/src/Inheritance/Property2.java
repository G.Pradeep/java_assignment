package Inheritance;

public class Property2 extends Property1 {
	
	public static void main(String[] args) {
		
		Property2 p2 = new Property2();
		p2.money();
		p2.watch();
		p2.mobile();
	}

}
