package Abstraction;

public class Santro extends Car implements BasicCar {
	 public void remoteStart() {
			// TODO Auto-generated method stub
		System.out.println("remotestarting the car");
		}
	 
	 public static void main(String[] args) {
		Santro s = new Santro();
		s.drive();
		s.stop();
		s.remoteStart();
		s.gearChange();
		s.music();
	}
	@Override
	public void gearChange() {
		System.out.println("changing gear in santro");
	}

	@Override
	public void music() {
		System.out.println("playing music in santro");
		
	}
 
}
