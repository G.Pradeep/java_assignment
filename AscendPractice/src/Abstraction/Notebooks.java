package Abstraction;

public class Notebooks extends Book {

	@Override
	public void write() {
		System.out.println("writing in notebook");

	}

	@Override
	public void read() {
		System.out.println("reading in notebook");

	}
	public void draw() {
		System.out.println("drawing in notebook");
	}
	
	public static void main(String[] args) {
		Notebooks note = new Notebooks();
		note.write();
		note.read();
		note.draw();
	}
}
