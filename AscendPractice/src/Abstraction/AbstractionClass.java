package Abstraction;

public abstract class AbstractionClass {

	// an abstract class is a class consist of both abstract and non abstract method
	// non abstract method is not compulsory it can have all the method as abstract method
	//abstact method is a method without body it just declared ( contain declaration)
	// it cannot be instantiated that is we cannot create object for the class
	
	public abstract void emplayeeId();// abstract method
	
	public void department() {// non abstract method
		// TODO Auto-generated method stub

	}
	
}
