package oops;

public class MainClass {
	public static void main(String[] args) {

		One a = new One();

		a.display();

		Two b = new Two();

		b.display();

		System.out.println(b.add(4, 2));

		System.out.println(b.add(5.,2.)); // polymorphism

		EncapTest encap = new EncapTest();

		encap.setName("pradeep");

		System.out.print("Name : " + encap.getName());

		TwoWheelers test = new Honda();

		test.run();

	}

}
