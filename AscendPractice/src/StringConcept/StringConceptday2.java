package StringConcept;

public class StringConceptday2 {

	public static void main(String[] args) {

		String s1 = "hello";// store in constant pool area
		String s2 = "hello";
		String s3 = new String("hello");// store in heap memory
		String s4 = new String("hello");
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);
		System.out.println(s4);

		s1 = s1.concat("java");
		System.out.println(s1);
		
		boolean a = s2.equals(s3);// equals() checks the contents and not the reference
		System.out.println(a);
		
		boolean b  = s2==s3;
		System.out.println(b);// it checks the contents as well as reference
	}

}
